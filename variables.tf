variable "vpc_cidr_block" {
  type        = string
  description = "Valor do block cidr da VPC"
  default     = "10.1.0.0/16"
}

variable "subnet_pub_cidr_block" {
  type        = string
  description = "Valor do block cidr da Subnet Pública"
  default     = "10.1.1.0/24"
}

variable "instance_name" {
  type        = list(string)
  description = "Nomes das instancias EC2"
  default     = ["Node1", "Node2", "Node3"]
}

variable "key" {
  type        = string
  description = "Nome da chave SSH"
  default     = "aws-gitlab-efc-key"
}

variable "ec2_type" {
  type        = string
  description = "Tipo da EC2"
  default     = "t2.medium"
}

variable "default_ingress" {
  type = map(object({ description = string, protocol = string, cidr_blocks = list(string) }))
  default = {
    22    = { description = "inbound para SSH", protocol = "tcp", cidr_blocks = ["0.0.0.0/0"] }
    80    = { description = "inbound para HTTP", protocol = "tcp", cidr_blocks = ["0.0.0.0/0"] }
    443   = { description = "inbound para HTTPS", protocol = "tcp", cidr_blocks = ["0.0.0.0/0"] }
    -1    = { description = "inbound para ICMP", protocol = "icmp", cidr_blocks = ["0.0.0.0/0"] }
    6443  = { description = "Kubernetes API Server", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    2379  = { description = "etcd server client API", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    2380  = { description = "etcd server client API", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    10250 = { description = "Kubelet API", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    10251 = { description = "kube-scheduler", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    10252 = { description = "kube-controller-manager", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    10255 = { description = "Read-only Kubelet API", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    6781  = { description = "CNI Weave Net", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    6782  = { description = "CNI Weave Net", protocol = "udp", cidr_blocks = ["10.1.1.0/24"] }
    6783  = { description = "CNI Weave Net", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    6784  = { description = "CNI Weave Net", protocol = "tcp", cidr_blocks = ["10.1.1.0/24"] }
    #30000-32767  = { description = "NodePort Services", protocol = "tcp", cidr_blocks = ["0.0.0.0/0"] }
  }
}