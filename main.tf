provider "aws" {
  region = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }

  }

  backend "s3" {
    region = "us-east-1"
    bucket = "terraform-testelab"
    key    = "terraform/lab/k8s-linuxtips/labk8s"
  }
}