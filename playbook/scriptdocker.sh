#!/bin/bash
# Instalar o  docker
curl -fsSL https://get.docker.com | bash

# Configurar o SystemD
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

# Criando o diretório do docker service
mkdir mkdir -p /etc/systemd/system/docker.service.d