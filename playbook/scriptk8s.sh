# Script preparatório para a inicialização do Cluster k8s
# Pré-instalando requisitos da instalação via gerenciador de pacotes debian
apt-get update && apt-get install -y apt-transport-https gnupg2 curl

# Adicionando chave do repo do k8s
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

# Incluindo repos k8s na lista de resources do apt-get
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list

# Instalando componentes do k8s
apt-get update && apt-get install -y kubeadm kubelet kubectl

# Ativando completion/etc/bash_completion.d/kubectl54.87.151.57
kubectl completion bash > /etc/bash_completion.d/kubectl

# Primeiro subir módulos do kernel, no ubuntu a maioria já está ativo
modprobe br_netfilter ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs

# Fazendo pull das imagens no nó Master para ele baixar os componentes do cluster
kubeadm config images pull

# aqui aplicaremos as configurações do pod network nos componentes do cluster o comando abaixo usa o script do weavenet
#kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"