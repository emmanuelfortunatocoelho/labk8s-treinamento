output "server_public_ip_1" {
  value = aws_instance.instancia-lab-k8s[0].public_ip
}

output "server_public_ip_2" {
  value = aws_instance.instancia-lab-k8s[1].public_ip
}

output "server_public_ip_3" {
  value = aws_instance.instancia-lab-k8s[2].public_ip
}