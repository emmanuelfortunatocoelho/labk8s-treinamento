# VPC (Default Route Table + Default Security Group Editados) + Subnet Publica + Internet GW

resource "aws_vpc" "vpc-lab-k8s" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "VPC do Lab K8s"
    Env  = "Treinamento"
  }
}

resource "aws_internet_gateway" "gw-lab-k8s" {

  vpc_id = aws_vpc.vpc-lab-k8s.id

  tags = {
    Name = "Gateway do Lab K8s"
    Env  = "Treinamento"
  }
}

resource "aws_default_route_table" "rt-lab-k8s" {
  depends_on = [
    aws_internet_gateway.gw-lab-k8s
  ]

  default_route_table_id = aws_vpc.vpc-lab-k8s.default_route_table_id


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw-lab-k8s.id
  }

  tags = {
    Name = "Route Table do Lab K8s"
    Env  = "Treinamento"
  }
}


resource "aws_subnet" "subnet-pub-lab-k8s" {

  vpc_id                  = aws_vpc.vpc-lab-k8s.id
  cidr_block              = var.subnet_pub_cidr_block
  map_public_ip_on_launch = true

  tags = {
    Name = "Subnet Publica do Lab K8s"
    Env  = "Treinamento"
  }
}

resource "aws_default_security_group" "sg-lab" {

  vpc_id = aws_vpc.vpc-lab-k8s.id

  dynamic "ingress" {
    for_each = var.default_ingress
    content {
      description = ingress.value["description"]
      from_port   = ingress.key
      to_port     = ingress.key
      protocol    = ingress.value["protocol"]
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "SG do Lab K8s"
    Env  = "Treinamento"
  }
}

resource "aws_instance" "instancia-lab-k8s" {

  count                  = length(var.instance_name)
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.ec2_type
  subnet_id              = aws_subnet.subnet-pub-lab-k8s.id
  key_name               = var.key
  vpc_security_group_ids = ["${aws_default_security_group.sg-lab.id}"]

  tags = {
    Name = var.instance_name[count.index]
    Env  = "Treinamento"
  }
}